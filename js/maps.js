var map = L.map('map').setView([51.505, -0.09], 13);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

// add Leaflet-Geoman controls with some options to the map  
map.pm.addControls({  
    position: 'topleft',  
    drawCircle: false,  
  });  